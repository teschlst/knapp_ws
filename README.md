# Order Optimizer

## Dependencies:
##### libyaml-cpp-dev

### How to build:
#### Inside the knapp_ws run:
source/opt/ros/foxy/setup.bash

colcon build

#### How to run:
#### Source the package with:
source install/setup.bash

#### Run the optimizer:
ros2 run OrderOptimizer OrderOptimizer /absolute/path/to/directory/containing/two/subfolders

#### Run the publisher:
ros2 run OrderOptimizer OrderPublisher /absolute/path/to/orders.yaml






