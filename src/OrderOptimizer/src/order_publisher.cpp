#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <fstream>

#include "boost/filesystem.hpp"
#include "yaml-cpp/yaml.h"
#include "rclcpp_components/component_manager.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "knapp_msgs/msg/next_order.hpp"

using namespace std::chrono_literals;


class OrderPublisher : public rclcpp::Node
{
  public:

    std::string cmd_argument_string; // stores the path to the file folder

    OrderPublisher(std::string cmd_argument_string) : Node("next_order_publisher")
    {
      this->cmd_argument_string = cmd_argument_string;
      pos_publisher_ = this->create_publisher<geometry_msgs::msg::PoseStamped>("currentPosition", 10);
      pos_timer_ = this->create_wall_timer(
      8000ms, std::bind(&OrderPublisher::pos_timer_callback, this));

      order_publisher_ = this->create_publisher<knapp_msgs::msg::NextOrder>("nextOrder", 10);
      order_timer_ = this->create_wall_timer(
      8000ms, std::bind(&OrderPublisher::order_timer_callback, this));


    }
  int cnt = 0;

  private:
    void order_timer_callback()
    {
      auto message = knapp_msgs::msg::NextOrder();

      YAML::Node config = YAML::LoadFile(cmd_argument_string);
      cnt++;
      
      message.order_id = config[cnt]["order"].as<std::uint32_t>();
      message.description = "Some description";
      order_publisher_->publish(message);
    }
    rclcpp::TimerBase::SharedPtr order_timer_;
    rclcpp::Publisher<knapp_msgs::msg::NextOrder>::SharedPtr order_publisher_;
    size_t order_count_;





    void pos_timer_callback()
    {
      auto message = geometry_msgs::msg::PoseStamped();
      message.pose.orientation.w = 0.0;
      message.pose.position.set__x(50.0);
      message.pose.position.set__y(50.0);
      message.pose.position.x = 50.0;
      message.pose.position.y = 50.0;
      message.pose.orientation.z = 0.0;
      

      
      //RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", "23");
      pos_publisher_->publish(message);
    }
    rclcpp::TimerBase::SharedPtr pos_timer_;
    rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr pos_publisher_;
    size_t pos_count_;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<OrderPublisher>(argv[1]));
  rclcpp::shutdown();
  return 0;
}