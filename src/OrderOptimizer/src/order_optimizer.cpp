#include <memory>
#include <vector>
#include <filesystem>
#include <iostream>
#include <string>
#include <glob.h>
#include <future>
#include <exception>
#include <chrono>
#include <cmath>
#include <bits/stdc++.h>

#include "string.h"
#include "yaml-cpp/yaml.h"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "knapp_msgs/msg/next_order.hpp"
#include "visualization_msgs/msg/marker.hpp"
#include "visualization_msgs/msg/marker_array.hpp"


using std::placeholders::_1;
using namespace std::chrono_literals;


class OrderOptimizer : public rclcpp::Node
{
public:
    std::string cmd_argument_string; // stores the path to the file folder
    YAML::Node order_details_file; // global details parsed from yaml
    visualization_msgs::msg::MarkerArray marker_array; // global marker array 

    double adjacency_array[100][100]; //array for path finding

    double amr_pos_x = 0.0; //global input loaction
    double amr_pos_y = 0.0; //global input locatoon


    struct partsStruct 
    {
        std::string partname = "Empty";
        int units = 1;
        double part_pos_x = 0.0;
        double part_pos_y = 0.0;
    };

    struct productsStruct 
    {
        int product_id = 0;
        std::vector<partsStruct> parts;
    };

    struct inputOrderStruct 
    {
        std::uint32_t order_id = 0;
        double start_pos_x = 0.0;
        double start_pos_y = 0.0;
        double goal_pos_x = 0.0;
        double goal_pos_y = 0.0;

        std::vector<productsStruct> products; 
    };

    struct nodesStruct  
    {
        std::vector<productsStruct> products;
        double pos_x = 0;
        double pos_y = 0;
    };

    OrderOptimizer(std::string cmd_argument_string) : Node("OrderOptimizer")
    {
        
        std::string path = cmd_argument_string + "/configuration/*";
        const std::vector<std::string> config_path = globVector(path);

        try
        {
            order_details_file = YAML::LoadFile(config_path[0]);
           
        }
        catch(const YAML::ParserException& ex)
        {
            std::cout << "Please enter a valid path" << ex.what() << std::endl;
        }
          
        this->cmd_argument_string = cmd_argument_string;

        amr_pos_sub_ = this->create_subscription<geometry_msgs::msg::PoseStamped>(
        "currentPosition", 10, std::bind(&OrderOptimizer::pos_callback, this, _1));

        order_sub_ = this->create_subscription<knapp_msgs::msg::NextOrder>(
        "nextOrder", 10, std::bind(&OrderOptimizer::order_callback, this, _1));

        marker_publisher_ = this->create_publisher<visualization_msgs::msg::MarkerArray>("order_path", 10);
        marker_timer_ = this->create_wall_timer(1000ms, std::bind(&OrderOptimizer::marker_timer_callback, this));
    }

private:
    std::string parameter_string_;

    void marker_timer_callback()
    {      
        visualization_msgs::msg::MarkerArray arry;
        arry = marker_array;
        marker_publisher_->publish(arry);
    }
    rclcpp::TimerBase::SharedPtr marker_timer_;
    rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr marker_publisher_;
    size_t marker_count_;

    void pos_callback(const geometry_msgs::msg::PoseStamped::SharedPtr msg)
    {
        //RCLCPP_INFO(this->get_logger(), "Pose: '%f'", msg->pose.position.x);
        amr_pos_x = msg->pose.position.x;
        amr_pos_y = msg->pose.position.y;
    }
    rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr amr_pos_sub_;

    void order_callback(const knapp_msgs::msg::NextOrder::SharedPtr msg) 
    {
        // RCLCPP_INFO(this->get_logger(), "Order Description: '%s'", msg->description.c_str());
        // RCLCPP_INFO(this->get_logger(), "Order ID: '%d'", msg->order_id);
          optimize(msg->order_id);
    }
  
    rclcpp::Subscription<knapp_msgs::msg::NextOrder>::SharedPtr order_sub_;


    //all the magic happens here
    //gets called by an incoming order
    void optimize(std::uint32_t order_id) 
    { 
        std::cout << "Incoming order with ID: " << order_id << std::endl;
        // extend the input string to search in the right folder
        std::string folder_str = cmd_argument_string + "/orders/*";
        // searches for all files within the folder path 
        std::vector<std::string> files = globVector(folder_str);

        //print all found files within the directory
        for(int i = 0; i < files.size(); i++)
        {
            //std::cout << files[i] << std::endl;
        }

        bool isValid = false;
        inputOrderStruct inputOrder; // all details to the order get stored in this struct

        std::vector<std::future<std::tuple<bool,inputOrderStruct>>> future_vector;
        
        // parse all files within the directory in different threads
        for(int i = 0; i < files.size(); i++)
        {
            //pushes the return values of the thread onto a vector    
            future_vector.push_back(std::async(std::launch::async, find_order, order_id, files[i]));
        }

        // check if the parsed files contain the order
        for(int i = 0; i < future_vector.size(); i++)
        {   
            std::tie(isValid, inputOrder) = future_vector[i].get();
            if(isValid == true)
            {
                //std::cout << "Found the details to the order" << std::endl;
                break;
            }   
        }

        inputOrder.start_pos_x = amr_pos_x;
        inputOrder.start_pos_y = amr_pos_y;

        //fill the inputOrder struct with information regarding the order and products
        find_order_parts(&inputOrder);
        erase_duplicates(&inputOrder);

        //find out in which sorage racks the order is contained
        std::vector<nodesStruct> nodeVector;
        nodeVector = createNodeStruct(&inputOrder);
        std::cout.precision(6);
        //print_order_data(inputOrder);
        //print_node_details(nodeVector);
        //print_node_details(nodesStr);

        //compute the adjacency matrix for path finding
        std::tuple<int,std::vector<nodesStruct>> out;
        out = compute_adjacency_matrix(&inputOrder, nodeVector);

        int size_array = std::get<0>(out);
        std::vector<nodesStruct> nodesStr = std::get<1>(out);

        //find the shortest path between the start position storage racks and the goal position
        std::vector<int> shortest_path = travllingSalesmanProblem(size_array,size_array-1);


        print_result(nodesStr, &inputOrder, shortest_path);

        create_marker_array(&inputOrder);
    }

    void print_order_data(inputOrderStruct inputOrder)
    {
        for(int i = 0; i < inputOrder.products.size(); i++)
        {
            std::cout << "Product ID: "<< inputOrder.products[i].product_id << std::endl;
            for(int j = 0; j < inputOrder.products[i].parts.size(); j++)
            {
                std::cout << "Part Name: "<< inputOrder.products[i].parts[j].partname << std::endl;
                std::cout << "Part Pos_x: "<< inputOrder.products[i].parts[j].part_pos_x << std::endl;
                std::cout << "Part Pos_y: "<< inputOrder.products[i].parts[j].part_pos_y << std::endl;
                std::cout << "Units: "<< inputOrder.products[i].parts[j].units << std::endl;
                
            }
            std::cout << std::endl;
        }    
    }

    void print_node_details(std::vector<nodesStruct> nodeVector)
    {
        std::cout << "Nodes to drive to: "<< nodeVector.size() << std::endl;
        for(int i = 0; i < nodeVector.size(); i++)
        {
            std::cout << "Rack at x: "<< nodeVector[i].pos_x << std::endl;
            std::cout << "Rack at y: "<< nodeVector[i].pos_y << std::endl;
            std::cout << "Has the following parts for the order stored: " << std::endl;
            for(int j = 0; j < nodeVector[i].products.size(); j++)
            {
                std::cout << "Product: "<< nodeVector[i].products[j].product_id << std::endl;
                for(int k = 0; k < nodeVector[i].products[j].parts.size(); k++)
                {
                    std::cout << "Part: "<< nodeVector[i].products[j].parts[k].partname << std::endl;
                    std::cout << "Units: "<< nodeVector[i].products[j].parts[k].units << std::endl;
                }
            }
            std::cout << std::endl;
        }
    }
    
    void print_result(std::vector<nodesStruct> nodesStr, inputOrderStruct* inputOrder, std::vector<int> shortest_path)
    {
        if(shortest_path[0] == 0) //print in forward direction
        {
            std::cout << "Working on order: " << inputOrder->order_id << std::endl;
            std::cout << "Assigning AMR at position x: " << nodesStr[0].pos_x << ", y: " << nodesStr[0].pos_y << " the job" << std::endl;
            for(int i = 0; i < nodesStr.size(); i++)
            {                
                for(int j = 0; j < nodesStr[shortest_path[i]].products.size(); j++)
                {
                    for(int k = 0; k < nodesStr[shortest_path[i]].products[j].parts.size(); k++)
                    {
                        std::cout << "Fetching " << nodesStr[shortest_path[i]].products[j].parts[k].partname << " units: '";
                        std::cout << nodesStr[shortest_path[i]].products[j].parts[k].units << "' ";
                        std::cout << "for product '" << nodesStr[shortest_path[i]].products[j].product_id << "' ";
                        std::cout << " at x: " << nodesStr[shortest_path[i]].products[j].parts[k].part_pos_x << "";
                        std::cout << ", y: " << nodesStr[shortest_path[i]].products[j].parts[k].part_pos_y << "" << std::endl;
                    }
                }  
            }
            std::cout << "Delivering to destination x: " << nodesStr[shortest_path.size() - 1].pos_x << ", y: " << nodesStr[shortest_path.size() - 1].pos_x << std::endl;
            std::cout << std::endl;
        }
        else
        {
            std::cout << "Working on order: " << inputOrder->order_id << std::endl;
            std::cout << "Assigning AMR at position x: " << nodesStr[0].pos_x << ", y: " << nodesStr[0].pos_y << " the job" << std::endl;
            for(int i = nodesStr.size() - 1; i >= 0; i--)
            {                
                for(int j = 0; j < nodesStr[shortest_path[i]].products.size(); j++)
                {
                    for(int k = 0; k < nodesStr[shortest_path[i]].products[j].parts.size(); k++)
                    {
                        std::cout << "Fetching " << nodesStr[shortest_path[i]].products[j].parts[k].partname << " units: '";
                        std::cout << nodesStr[shortest_path[i]].products[j].parts[k].units << "' ";
                        std::cout << "for product '" << nodesStr[shortest_path[i]].products[j].product_id << "' ";
                        std::cout << " at x: " << nodesStr[shortest_path[i]].products[j].parts[k].part_pos_x << "";
                        std::cout << ", y: " << nodesStr[shortest_path[i]].products[j].parts[k].part_pos_y << "" << std::endl;
                    }
                }  
            }
            std::cout << "Delivering to destination x: " << nodesStr[shortest_path.size() - 1].pos_x << ", y: " << nodesStr[shortest_path.size() - 1].pos_x << std::endl;
            std::cout << std::endl;
        }
    }


    std::tuple<int,std::vector<nodesStruct>> compute_adjacency_matrix(inputOrderStruct* inputOrder, std::vector<nodesStruct> nodeVector)
    {
        nodesStruct start;
        nodesStruct goal;

        start.pos_x = inputOrder->start_pos_x;
        start.pos_y = inputOrder->start_pos_y;
        goal.pos_x = inputOrder->goal_pos_x; //900;
        goal.pos_y = inputOrder->goal_pos_y; //900;

        std::vector<nodesStruct> nodes;

        nodes.push_back(start);
        for(int i = 0; i < nodeVector.size(); i++)
        {
            nodes.push_back(nodeVector[i]);
        }
        nodes.push_back(goal);

        for(int i = 0; i < 100; i++) // set global array to 0
        {
            for(int j = 0; j < 100; j++)
            {   
                adjacency_array[i][j] = 0;
            }
        }

        //init start point
        for(int i = 0; i < nodes.size(); i++)
        {
            for(int j = 0; j < nodes.size(); j++)
            {   
                double distance = sqrt(pow((nodes[i].pos_x - nodes[j].pos_x), 2)  + pow((nodes[i].pos_y - nodes[j].pos_y), 2));
                adjacency_array[i][j] = distance;
                adjacency_array[j][i] = distance;
            }
        }

        for(int i = 0; i < nodes.size(); i++) // add dummy node to extend the solution for start and stop point travelling salesman problem
        {
            for(int j = 0; j < nodes.size(); j++)
            {   
                if(j == 0 | j == nodes.size()-1)
                {
                    adjacency_array[nodes.size()][j] = 0;
                    adjacency_array[j][nodes.size()] = 0;
                }
                else
                {   //fake node "infinity distance to all other nodes than the start and end node"
                    adjacency_array[nodes.size()][j] = 9999999;
                    adjacency_array[j][nodes.size()] = 9999999;
                }
            }
        }
        return std::make_tuple(nodes.size() +1, nodes);
    }



    // implementation of traveling Salesman Problem 
    std::vector<int> travllingSalesmanProblem(int start, int array_size) 
    { 
    // store all vertex apart from source vertex 
        std::vector<int> vertex; 
        std::vector<int> finalPath;
        for (int i = 0; i < array_size; i++) 
            if (i != start) 
                vertex.push_back(i); 
    
        // store minimum weight Hamiltonian Cycle. 
        int min_path = INT_MAX; 
        do { 
    
            // store current Path weight(cost) 
            int current_pathweight = 0; 
            
            // compute current path weight 
            int k = start; 
            for (int i = 0; i < vertex.size(); i++) 
            { 
                current_pathweight += (int)adjacency_array[k][vertex[i]]; 
                k = vertex[i]; 
            } 
            current_pathweight += (int)adjacency_array[k][start]; 
    
            // update minimum 
            if(current_pathweight <= min_path)
            {
                finalPath = vertex;
            }
            min_path = std::min(min_path, current_pathweight); 
            
        } while (std::next_permutation(vertex.begin(), vertex.end())); 
  
        return finalPath; 
    } 
    
            




    // this function searches for files within a certain directory and returns it as vector string
    std::vector<std::string> globVector(const std::string& pattern) const
    {
        glob_t glob_result;
        int return_value = glob(pattern.c_str(),GLOB_TILDE,NULL,&glob_result);
        if(return_value != 0) 
        {
            globfree(&glob_result);
            std::cout << "failed to parse the files from the entered path: " << pattern << std::endl;
            std::cout << "please check the path " << std::endl;
        }
        std::vector<std::string> files;
        for(unsigned int i=0;i<glob_result.gl_pathc;++i)
        {
            files.push_back(std::string(glob_result.gl_pathv[i]));
        }
        globfree(&glob_result);
        return files;
    }


    // deletes all duplicates of the parts and increments the units variable
    void erase_duplicates(inputOrderStruct* instruct) const
    {   
        int cnt = 0;
        for(int i = 0; i < instruct->products.size(); i++)
        {
            for(int j = 0; j < instruct->products[i].parts.size(); j++)
            {
                std::string partname = instruct->products[i].parts[j].partname;
                for(int k = 0; k < instruct->products[i].parts.size(); k++)
                {
                    if(k <= j) // do nothing first element will alway be with the same name
                    {

                    }
                    else
                    {
                        if(partname == instruct->products[i].parts[k].partname)
                        {
                            instruct->products[i].parts[j].units++;
                            std::vector<partsStruct>::iterator iter = instruct->products[i].parts.begin();
                            iter = iter + k;
                            instruct->products[i].parts.erase(iter);
                            k = 0;
                        }
                    }
                }
            }
        }
    }

    // this function searches fot a given order id in the passed path
    // if it finds the order details it will return a struct with the details
    static std::tuple<bool,inputOrderStruct> find_order(std::uint32_t order_id, std::string path)
    {
        inputOrderStruct inputOrder;
        inputOrder.order_id = order_id;
        YAML::Node config = YAML::LoadFile(path);

        std::vector<std::uint32_t> product_vector;
        productsStruct proStruct;

        bool isOrderValid = false;
        for(int i = 0; i < config.size(); i++)
        {
            if(config[i]["order"].as<std::uint32_t>() == order_id)
            {   
                std::cout << "Found the product in files " << std::endl;
                isOrderValid = true;
                inputOrder.goal_pos_x = config[i]["cx"].as<std::double_t>();
                inputOrder.goal_pos_y = config[i]["cy"].as<std::double_t>();

                for(int j = 0; j < config[i]["products"].size(); j++)
                {
                    proStruct.product_id = config[i]["products"][j].as<u_int32_t>();
                    //std::cout << "Product " << j << ": " << product_vector[j] << std::endl;
                    inputOrder.products.push_back(proStruct); 
                }
                break;
            }
        }
        return std::make_tuple(isOrderValid, inputOrder);
    }



    // find the parts from the details 
    void find_order_parts(inputOrderStruct* inputOrder) const
    {
        partsStruct parStruct;
        // search the products and the parts and add them to the structure
        for(int i = 0; i < inputOrder->products.size(); i++)
        {
            for(int j = 0; j < order_details_file.size(); j++)
            {
                if(inputOrder->products[i].product_id == order_details_file[j]["id"].as<std::uint32_t>())
                {

                    for(int k = 0; k < order_details_file[j]["parts"].size(); k++)
                    {   
                        parStruct.part_pos_x = order_details_file[j]["parts"][k]["cx"].as<double>();
                        parStruct.part_pos_y = order_details_file[j]["parts"][k]["cy"].as<double>();
                        parStruct.partname = order_details_file[j]["parts"][k]["part"].as<std::string>();
                        inputOrder->products[i].parts.push_back(parStruct);
                    }
                    break;
                } 
            }  
        }
    }

    // create a node struct which contains the positions of the storage racks needed for the order
    std::vector<nodesStruct> createNodeStruct(inputOrderStruct* instruct)
    {
        std::vector<nodesStruct> nodeVector;
        nodesStruct nodeStruc;
        productsStruct productsStruct;
        partsStruct parts;


        // search the products and the parts and add them to the structure
        for(int i = 0; i < instruct->products.size(); i++)
        {
            for(int j = 0; j < instruct->products[i].parts.size(); j++)
            {   
                bool flag = false;
                for(int k = 0; k < nodeVector.size(); k++) // check if parts have the same rack
                {
                    if(nodeVector[k].pos_x == instruct->products[i].parts[j].part_pos_x)
                    {
                        //part is stored at an already existing location
                        productsStruct.product_id = instruct->products[i].product_id;
                        parts.part_pos_x = instruct->products[i].parts[j].part_pos_x;
                        parts.part_pos_y = instruct->products[i].parts[j].part_pos_y;
                        parts.partname = instruct->products[i].parts[j].partname;
                        parts.units = instruct->products[i].parts[j].units;

                        productsStruct.parts.push_back(parts);
                        nodeVector[k].products.push_back(productsStruct);
                        flag = true;
                    }
                }
                if(flag == false) // if the rack is not stored jet create a new
                {   
                    nodeStruc.pos_x = instruct->products[i].parts[j].part_pos_x;
                    nodeStruc.pos_y = instruct->products[i].parts[j].part_pos_y;
                    productsStruct.product_id = instruct->products[i].product_id;
                    parts.part_pos_x = instruct->products[i].parts[j].part_pos_x;
                    parts.part_pos_y = instruct->products[i].parts[j].part_pos_y;
                    parts.partname = instruct->products[i].parts[j].partname;
                    parts.units = instruct->products[i].parts[j].units;

                
                    productsStruct.parts.push_back(parts);
                    nodeStruc.products.push_back(productsStruct);
                    nodeVector.push_back(nodeStruc);
                }

                productsStruct.parts.clear();
                nodeStruc.products.clear();
            }
        } 
        return nodeVector;   
    }


    // creates a marker array which publishes the parts, the start and end position
    void create_marker_array(inputOrderStruct* instruct)
    {
        int cnt = 0;
        marker_array.markers.clear();
        
        marker_array.markers.push_back(create_marker(instruct->goal_pos_x/50, instruct->goal_pos_y/50, "CYLINDER", true, cnt)); //goal_pos
        cnt++;
        marker_array.markers.push_back(create_marker(instruct->start_pos_x/50, instruct->start_pos_y/50, "CUBE", true, cnt)); //start_pos
        cnt++;
        for(int i = 0; i < instruct->products.size(); i++)
        {
            for(int j = 0; j < instruct->products[i].parts.size(); j++)
            {   
                marker_array.markers.push_back(create_marker(instruct->products[i].parts[j].part_pos_x/50, instruct->products[i].parts[j].part_pos_y/50, "CYLINDER", false, cnt)); 
                cnt++;
            }
        }
        
    }

    visualization_msgs::msg::Marker create_marker(double pos_x, double pos_y, std::string shape, bool color, int cnt)
    {

            visualization_msgs::msg::Marker marker;
            marker.header.frame_id = "map";
            marker.header.stamp = this->get_clock()->now();
            marker.ns = "my_namespace";
            marker.id = cnt;

            if(shape == "CUBE")
                marker.type = visualization_msgs::msg::Marker::CUBE;
            if(shape == "CYLINDER")
                marker.type = visualization_msgs::msg::Marker::CYLINDER; 

            marker.action = visualization_msgs::msg::Marker::ADD;
            marker.pose.position.x = pos_x;
            marker.pose.position.y = pos_y;
            marker.pose.position.z = 1;
            marker.pose.orientation.x = 0.0;
            marker.pose.orientation.y = 0.0;
            marker.pose.orientation.z = 0.0;
            marker.pose.orientation.w = 1.0;
            marker.scale.x = 1;
            marker.scale.y = 1;
            marker.scale.z = 1;
            marker.color.a = 1.0; 
            marker.color.r = 0.0;
            marker.color.g = 1.0;
            if(color == true)
                marker.color.b = 0.0;
            else 
                marker.color.b = 1.0;

            return marker;
    }
};

int main(int argc, char * argv[])
{
    if(!argv[1] == NULL)
    {
        static const std::string cmd_argument_string = argv[1];
        //std::cout << "name of program: " << argv[1] << '\n' ;
        rclcpp::init(argc, argv);
        rclcpp::spin(std::make_shared<OrderOptimizer>(cmd_argument_string));
        rclcpp::shutdown();
    }
    else
        std::cout << "Please append path to the folder with the yaml files" << std::endl;

    return 0;
}